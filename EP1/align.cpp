#include<iostream>
#include<string>
#include<algorithm>
#include<vector>

using namespace std;

const int MATCH = 1, MISMATCH = -1, GAP = -2, SPACE = 0, SYMBOL = 1;
string als, alt;
int counter;

int p(char i, char j) {
    if (i == j) return MATCH;
    else return MISMATCH;
}

/* Algorithm for similarity in linear space.
In the end, a[n] contains sim(s, t). */
void bestScore(string s, string t, vector<int> &a) {

    int m, n;
    m = s.size();
    n = t.size();

    for (int i = 0; i <= n; i++) a.push_back(i * GAP);
    for (int i = 1; i <= m; i++) {
        int old = a[0];
        a[0] = i * GAP;
        for (int j = 1; j <= n; j++) {
            int temp = a[j];
            a[j] = max(a[j] + GAP, old + p(s[i-1], t[j-1]));
            a[j] = max(a[j], a[j-1] + GAP);
            old = temp;
        }
    }
}

/* Algorithm for similarity in quadratic space.
In the end, a[n][m] contains sim(s, t). */
void similarity(string s, string t, vector< vector<int> > &a) {

    int m, n;
    m = s.size();
    n = t.size();

    for (int i = 0; i <= m; i++) a[i][0] = i * GAP;
    for (int j = 1; j <= n; j++) a[0][j] = j * GAP;

    for (int i = 1; i <= m; i++) {
        for (int j = 1; j <= n; j++) {
            a[i][j] = max(a[i-1][j] + GAP, a[i][j-1] + GAP);
            a[i][j] = max(a[i][j], a[i-1][j-1] + p(s[i-1], t[j-1]));
        }
    }
}

/* Algorithm for alignment in quadratic space.
In the end, als and alt contains the aligned strings. */
void basicAlign(string s, string t, int i, int j, vector< vector<int> > &a) {

    if((i == 0) && (j == 0)) return;
    else if ((i > 0) && a[i][j] == a[i-1][j] + GAP) {
        basicAlign(s, t, i-1, j, a);
        als[counter] = s[i-1];
        alt[counter++] = '-';
    }
    else if ((i > 0) && (j > 0) && a[i][j] == a[i-1][j-1] + p(s[i-1], t[j-1])) {
        basicAlign(s, t, i-1, j-1, a);
        als[counter] = s[i-1];
        alt[counter++] = t[j-1];
    }
    else {
        basicAlign(s, t, i, j-1, a);
        als[counter] = '-';
        alt[counter++] = t[j-1];
    }
}

/* Algorithm for alignment in linear space.
In the end, als and alt contains the aligned strings. */
void align(string s, string t, int a, int b, int c, int d) {


    string cs, ct;
    cs = s.substr(a - 1, b - a + 1);
    ct = t.substr(c - 1, d - c + 1);

    //Uses basicAlign() for small cases
    if((cs.size() <= 1) || (ct.size() <= 1)) {
        vector< vector<int> > basic_sim(cs.size() + 1, vector<int>(ct.size() + 1));
        similarity(cs, ct, basic_sim);
        basicAlign(cs, ct, cs.size(), ct.size(), basic_sim);
        return;
    }

    int i = (a + b) / 2;
    vector<int> pref, suff;
    string s_rev = s, t_rev = t;
    reverse(s_rev.begin(), s_rev.end());
    reverse(t_rev.begin(), t_rev.end());

    bestScore(s.substr(a - 1, i - a + 1), ct, pref);
    bestScore(s_rev.substr(i, b - i - 1), t_rev.substr(c - 1, d - c + 1), suff);

    int posmax = c - 1, typemax = SPACE, vmax;
    vmax = pref[c-1] + GAP + suff[c-1];

    for (int j = c; j <= d; j++) {
        if(pref[j-1] + p(s[i-1], t[j-1]) + suff[j] > vmax) {
            posmax = j;
            typemax = SYMBOL;
            vmax = pref[j-1] + p(s[i-1], t[j-1]) + suff[j];
        }
        if(pref[j] + GAP + suff[j] > vmax) {
            posmax = j;
            typemax = SPACE;
            vmax = pref[j] + GAP + suff[j];
        }
    }

    if(typemax == SPACE) {
        align(s, t, a, i - 1, c, posmax);
        als[counter] = s[i-1];
        alt[counter++] = '-';
        align(s, t, i + 1, b, posmax + 1, d);
    }
    else {
        align(s, t, a, i - 1, c, posmax - 1);
        als[counter] = s[i-1];
        alt[counter++] = t[posmax-1];
        align(s, t, i + 1, b, posmax + 1, d);
    }

}

int main(int argc, char const *argv[]) {

    string s, t;
    als = alt = "";

    cin >> s >> t;

    int len = max(s.size(), t.size());
    for (int i = 0; i < len; i++) {
        als.append("x");
        alt.append("x");
    }

    counter = 0;

    vector< vector<int> > basic_sim(s.size() + 1, vector<int>(t.size() + 1));
    similarity(s, t, basic_sim);
    basicAlign(s, t, s.size(), t.size(), basic_sim);

    cout << "Resposta 1:" << endl << als << endl << alt << endl;

    for (int i = 0; i < len; i++) {
        als[i] = 'x';
        alt[i] = 'x';
    }
    counter = 0;
    align(s, t, 1, s.size(), 1, t.size());

    cout << "Resposta 2:" << endl << als << endl << alt << endl;

    return 0;
}
