# Alinhamento ótimo de 2 sequências

Neste programa é implementado o algoritmo de alinhamento local ótimo entre
duas sequências s e t.

## Como executar o código

Para compilar o código em c++, execute:

```
make
```

Este comando irá compilar o código fonte:

* align.cpp

E irá gerar o executável:

* align

Para rodar o programa, é só digitar no terminal o comando:

```
./align
```

Então deve-se digitar as duas sequências que se deseja obter o alinhamento,
uma em cada linha, conforme o exemplo:

```
TCCGA
GTCTG
```

Com isso, o programa retorna duas respostas

* Resposta 1
Esta é a resposta dada pelo algoritmo quadrático da Figura 3.3 do livro,
usando a pontuação dada pelo algoritmo da Figura 3.2 do livro.

* Resposta 2
Esta é a resposta dada pelo algoritmo linear da Figura 3.6 do livro,
usando a pontuação dada algoritmo da Figura 3.5 do livro.


## Bibliografia

J. Meidanis and J.C. Setubal, Introduction to Computacional Molecular Biology,
PWS Publishing Company, Boston, 1997.
