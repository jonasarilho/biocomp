# Alinhamento ótimo de sequências múltiplas

Este programa escrito em C++ lê da linha de comando três inteiros r, q e g,
um inteiro k e k strings S1, S2, . . . , Sk correspondentes às k
sequências em questão.
O programa imprime um alinhamento ótimo e sua pontuação.

## Como executar o código

Para compilar o código em c++, execute:

```
make
```

Este comando irá compilar o código fonte:

* align.cpp

E irá gerar o executável:

* align

Para rodar o programa, é só digitar no terminal o comando:

```
./align 1 0 -1 3 ATC CGGA CACT
```

Passando os argumentos necessários, que são:

* 3 inteiros r, q e g;

* 1 inteiro k;

* k strings S1, S2, . . . , Sk;

Com isso, o programa retorna:

* Score:

Esta é a pontuação do alinhamento ótimo entre as k sequências.

* Alinhamento:

São k linhas com o alinhamento ótimo entre as sequências.

Para facilitar os testes, é só rodar o seguinte comando:

```
make teste
```

Este comando irá compilar o programa e rodar 2 testes simples nele,
gerando o seguinte resultado:

```
./align 1 0 -1 3 ATC CGGA CACT
score: 0
-ATC
CGGA
CACT
./align 1 0 -1 3 MQPILLL MLRLL MKILLL
score: 7
MQPILLL
M--LRLL
M-KILLL
```
E para testar o tempo que leva para calcular o alinhamento ótimo da leucina,
é só rodar o seguinte comando:

```
make leucina
```

Este comando irá compilar o programa e rodar o teste da leucina, que é bem grande,
e para mim acabou gerando o seguinte resultado:

```
time -f "tempo = %E"  \
./align 1 0 -1 4  \
GTCAGGATGGCCGAGTGGTCTAAGGCGCCAGACTCAAGT \
GTCAGGATGGCCGAGTGGTCTAAGGCGCCAGACTCAAGG \
GCCTCCTTAGTGCAGTAGGTAGCGCATCAGTCTCAAAA \
GTCAGGATGGCCGAGCAGTCTTAAGGCGCTGCGTTCAAAT
score: 150
GTCAGGATGGCCGAGTGGTCT-AAGGCGCCAGACTCAAGT
GTCAGGATGGCCGAGTGGTCT-AAGGCGCCAGACTCAAGG
GCCTCCTTAGTGCAGTAGG-T-AGCGCATCAGTCTCAAAA
GTCAGGATGGCCGAGCAGTCTTAAGGCGCTGCGTTCAAAT
tempo = 3:27.61
```

No final, para excluir o executável gerado, é só rodar o seguinte comando:

```
make clean
```

## Bibliografia

J. Meidanis and J.C. Setubal, Introduction to Computacional Molecular Biology,
PWS Publishing Company, Boston, 1997.
