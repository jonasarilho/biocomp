#include<iostream>
#include<string>
#include<algorithm>
#include<vector>
#include<climits>
#include<map>

using namespace std;

const int MENOS_INF = -100; //INT_MIN;
int MATCH, MISMATCH, GAP;

/* Match or mismatch function.*/
int p(char i, char j) {
    if (i == j) return MATCH;
    else return MISMATCH;
}

/* Algorithm for creating delta mask.
In the end, d receives 2^k - 1 binary masks.*/
void create_mask(vector<vector <int> > &d, vector <int> mask, int k, int i, int j) {
    if (i == k) {
      if (j) d.push_back(mask);
      return;
    }
    else {
        create_mask(d, mask, k, i+1, j);
        mask[i] = 1;
        create_mask(d, mask, k, i+1, 1);
    }
}


/* Algorithm for score of multiple sequences.
In the end, m returns the score.*/
int gsim(map<vector<int>,int> &memo,
    vector<string> &s, vector<int> &ind,
    const vector<vector<int> > &d) {

    int m, n = s.size();
    for (int i = 0; i < n; i++) if(ind[i] < 0) return MENOS_INF;

    if (memo.find(ind) != memo.end()) return memo[ind];

    m = MENOS_INF;
    for (int i = 0; i < d.size(); i++) {
        vector<int> new_ind(n);
        for (int j = 0; j < n; j++) new_ind[j] = ind[j] - d[i][j];

        int sum = 0;
        for (int j = 0; j < n; j++) {
            for (int k = j + 1; k < n; k++) {
                if(d[i][j] == d[i][k] == 1) {
                    sum += p(s[j][new_ind[j]],s[k][new_ind[k]]);
                }
                else if (d[i][j] != d[i][k]) sum += GAP;
            }
        }
        m = max(m, gsim(memo,s,new_ind,d) + sum);
    }

    memo[ind] = m;
    return m;
}

/* Algorithm for alignment of multiple sequences.
Prints the aligned sequences on cout.*/
void backtrace(map<vector<int>,int> &memo, vector<string> &s,
    const vector<vector<int> > &d) {

    int n = s.size();
    vector<string> vs(n);

    vector<int> ind(n);
    for (int j = 0; j < n; j++) ind[j] = s[j].length();
    int score = memo[ind];
    bool ok = true, in_range = true;
    while (ok) {
        for (int i = 0; i < d.size(); i++) {
            vector<int> new_ind(n);
            for (int j = 0; j < n; j++) {
                new_ind[j] = ind[j] - d[i][j];
                if(new_ind[j] < 0) in_range = false;
            }
            if(!in_range) {
                in_range = true;
                continue;
            }

            int sum = 0;
            for (int j = 0; j < n; j++) {
                for (int k = j + 1; k < n; k++) {
                    if(d[i][j] == d[i][k] == 1) {
                        sum += p(s[j][new_ind[j]],s[k][new_ind[k]]);
                    }
                    else if (d[i][j] != d[i][k]) sum += GAP;
                }
            }
            if (score == memo[new_ind] + sum) {
                for (int j = 0; j < n; j++) {
                    if(d[i][j] == 1) vs[j].append(s[j], ind[j] - 1, 1);
                    else vs[j].append("-");
                }
                for (int j = 0; j < n; j++) ind[j] = ind[j] - d[i][j];
                score = memo[ind];
                ok = false;
                for (int j = 0; j < n; j++) {
                    if(ind[j] > 0){
                        ok = true;
                        break;
                    }
                }
                break;
            }
        }
    }
    for (int j = 0; j < vs.size(); j++) {
        reverse(vs[j].begin(), vs[j].end());
        cout << vs[j] << endl;
    }
}

int main(int argc, char const *argv[]) {

    MATCH = atoi(argv[1]);
    MISMATCH = atoi(argv[2]);
    GAP = atoi(argv[3]);

    int k = atoi(argv[4]);

    vector<string> s(k);
    for (int i = 0; i < k; i++) s[i] = argv[5+i];

    vector<vector<int> > d;
    vector<int> mask(k);
    create_mask(d, mask, k, 0, 0);
    reverse(d.begin(), d.end());

    map<vector<int>,int> mult;
    vector<int> v(k);
    for (int i = 0; i < k; i++) v[i] = 0;
    mult[v] = MISMATCH;

    for (int i = 0; i < k; i++) v[i] = s[i].length();
    int ans = gsim(mult, s, v, d);
    cout << "score: " << ans << endl;
    backtrace(mult, s, d);

    return 0;
}
