# Alinhamento ótimo de sequências múltiplas

Este programa escrito em C++ resolve o seguinte problema:

São dados (na linha de comando) um limiar inteiro t e um conjunto com várias strings, fragmentos de orientação conhecida. Pede-se que se reconstrua uma supersequência comum do qual todos os fragmentos são segmentos conexos ao nível t.

## Como executar o código

Para compilar o código em c++, execute:

```
make
```

Este comando irá compilar o código fonte:

* assembly.cpp

E irá gerar o executável:

* assembly

Para rodar o programa, é só digitar no terminal o comando:

```
./assembly t f1 f2 ... fn
```


Passando os argumentos necessários, que são:

* 1 inteiros t;

* n strings f1, f2, . . . , fn;

Com isso, o programa retorna:

* Caminho:

Esta é o caminho encontrado.

* Fragmentos:

São n - 1 linhas com os fragmentos que se sobrepõem nas strings.

Para facilitar os testes, é só rodar o seguinte comando:

```
make teste
```

Este comando irá compilar o programa e rodar um testes simples nele,
que é o teste do exemplo 4.6 do livro:

* S = AGTATTGGCAATCGATGCAAACCTTTTGGCAATCACT

* w = AGTATTGGCAATC

* Z = AATCGATG

* u = ATGCAAACCT

* x = CCTTTTGG

* y= TTGGCAATCACT

E irá gerar o seguinte resultado:

```
./assembly 3 AGTATTGGCAATC AATCGATG ATGCAAACCT CCTTTTGG TTGGCAATCACT
caminho:
01234
AATC
ATG
CCT
TTGG
```

No final, para excluir o executável gerado, é só rodar o seguinte comando:

```
make clean
```

## Bibliografia

J. Meidanis and J.C. Setubal, Introduction to Computacional Molecular Biology,
PWS Publishing Company, Boston, 1997.
