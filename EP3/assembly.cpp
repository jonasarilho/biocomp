#include<iostream>
#include<string>
#include<algorithm>
#include<vector>
#include<utility>
#include<map>

using namespace std;

/* Algorithm to find the largest overlap comparing
the suffix of string s to the prefix of string t.
Returns an overlap between s and t or an empty string. */
string overlap(string s, string t) {

    for (int i = 0; i < s.length(); i++) {
        string si = s.substr(i);
        string ti = t.substr(0, s.length() - i);
        if(si == ti) return si;
    }

    return "";
}

/* Algorithm to find a Hamiltonian Path on an overlap Graph
Returns a valid path or an empty vector. */
vector<int> find_Hamiltonian_Path(map <int, vector <pair <int, string> > > &G, int n){
        vector<int> v;
        for(int i=0; i<n; i++)
            v.push_back(i);
        do{
            bool valid=true;
            for(int i=0; i<v.size()-1; i++){
                valid=false;
                for (auto a : G[v[i]]) {
                    if(a.first == v[i+1]){
                        valid=true;
                        break;
                    }
                }
                if(!valid) break;
            }
            if(valid) return v;
        } while(next_permutation(v.begin(), v.end()));

        vector<int> empty_vector;
        return empty_vector;
}


int main(int argc, char const *argv[]) {

    int THRESHOLD = atoi(argv[1]);

    int n = argc - 2;
    vector<string> fragments;

    for (int i = 2; i < argc; i++) {
        string s(argv[i]);
        fragments.push_back(s);
    }

    map <int, vector <pair <int, string> > > G;

    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {
            string s = overlap(fragments[i], fragments[j]);
            if(s.length() >= THRESHOLD) G[i].push_back(make_pair(j, s));
            s = overlap(fragments[j], fragments[i]);
            if(s.length() >= THRESHOLD) G[j].push_back(make_pair(i, s));
        }
    }

    vector<int> path = find_Hamiltonian_Path(G, n);
    if (path.empty()) cout << "nao existe passeio Hamiltoniano" << endl;
    else {
        cout << "caminho:" << endl;
        for (int i = 0; i < n; i++) cout << path[i];
        cout << endl;
    }

    for (int i = 0; i < n; i++) {
        for (auto a : G[path[i]]) {
            if(a.first == path[i+1]){
                cout << a.second << endl;
            }
        }
    }


    return 0;
}
